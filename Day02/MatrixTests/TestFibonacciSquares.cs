using Day02Tasks;

namespace MatrixTests;

[TestClass]
public class TestFibonacciSquares
{


    //lyubo test methods
    [TestMethod]
    public void TestFibonacciSearchWhenTrueBigSqr()
    {
        //Assign
                         //�� ����
        int[,] matrix = { {  1,  1,  2,  3 },
                          {144,  6,  7,  5 },
                          { 89,  6,  7,  8 },
                          { 55, 34, 21, 13 }};

        //Act
        bool result = TaskSolutions.HasFibonocciOrder(matrix);

        //Assert
        Assert.IsTrue(result);
    }

    [TestMethod]
    public void TestFibonacciSearchWhenTrueInMiddle()
    {
        //Assign
                            //�� �������
        int[,] matrix = { { 2,  1,  0, 3 },
                          { 9, 13, 21, 5 },
                          {12, 55, 34, 4 },
                          {55,  7, 21, 9 }};

        //Act
        bool result = TaskSolutions.HasFibonocciOrder(matrix);

        //Assert
        Assert.IsTrue(result);
    }

}