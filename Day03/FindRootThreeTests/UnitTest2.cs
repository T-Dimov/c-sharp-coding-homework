﻿using findRootThree;
namespace FindRootThreeTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void WorksCorrectWithBigNumber()
        {
            // Arrange
            int input = 512;
            int expected = 8;
            FindRootClass test = new FindRootClass();
            // Act
            int actual = test.FindRoot3(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}