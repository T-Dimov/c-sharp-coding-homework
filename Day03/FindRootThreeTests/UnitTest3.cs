﻿using findRootThree;
namespace FindRootThreeTests
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void WorksCorrectZero()
        {
            // Arrange
            int input = 0;
            int expected = 0;
            FindRootClass test = new FindRootClass();
            // Act
            int actual = test.FindRoot3(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}