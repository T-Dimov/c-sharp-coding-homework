﻿
using System.Runtime.CompilerServices;

namespace exprExpNS
{
    public class ExpressionExpansion
    {
        public static void Main()
        {
            Console.Write("Enter Expression: ");
            string input = Console.ReadLine();
            ExpressionExpansion expr = new ExpressionExpansion();
            
            showResult(expr.expand(input));
        }
        public string expand(string input)
        {
            string result = "";
            double repeatTimes = 0;
            for (int i = 0; i < input.Length; i++)
            {
                string strToRepeat = "";
                if (input[i] == '(')
                {

                    while (input[i + 1] != ')' && input[i + 1] != '(')
                    {
                        if (Char.GetNumericValue(input[i + 1]) != -1)
                        {
                            int index = input.Substring(i + 1).Length -1;
                            int subStringLength = index - i + 1;
                            strToRepeat += expand(input.Substring(i + 1));
                            i += subStringLength;
                            continue;
                        }

                        strToRepeat += input[i + 1];
                        i++;
                    }

                    for (int j = 0; j < repeatTimes; j++)
                    {
                        result += strToRepeat;
                    }
                    i++;
                    continue;
                }
                repeatTimes = Char.GetNumericValue(input[i]);
                if (repeatTimes == -1 && input[i] != ')')
                {
                    result += input[i];
                }
                continue;
            }
            return result;
        }
        public static void showResult(string result)
        {
            Console.WriteLine(result);
        }
    }
}