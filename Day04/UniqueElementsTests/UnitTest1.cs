using uniqueElementsNS;
namespace UniqueElementsTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void worksWithEmptyArray()
        {
            //Arrange
            List<int> input = new List<int> { };
            List<int> expected = new List<int> { };

            UniqueElementsClass result = new UniqueElementsClass();
            //Act
            var actual = result.uniqueElements(input);
            //Assert
            Assert.AreEqual(expected.Count,actual.Count);
        }
    }
}